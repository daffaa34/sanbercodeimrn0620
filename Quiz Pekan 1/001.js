console.log("Soal Balik String")


const balikString = (str) => {
     let temp = '';
     for (let i = str.length - 1; i >= 0; i--) {
         temp += str[i];
     }
     return temp;
 };
 
 console.log(balikString("abcde")) // edcba
 console.log(balikString("rusak")) // kasur
 console.log(balikString("racecar")) // racecar
 console.log(balikString("haji")) // ijah
 

console.log("Soal Palindrom")

const palindrome = (s = "", l = 0, r = s.length - 1) =>
  r - l < 2
    ? true
    : s[l] === s[r] && palindrome (s, l + 1, r - 1)

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false);


console.log("Soal Bandingkan Angka")

