var nilaiarray = []
var total = []

function sum(a1, b1, c1) {
    if (a1 == null) {

        return 0

    }
    if (b1 == null && c1 == null) {

        return a1

    }
   
    if (a1 < b1) {
        if (c1 == null) {
            c1 = 1
        }
        nilaiarray = []
        for (let index = a1; index <= b1; index += c1) {
            nilaiarray.push(index)
            total = nilaiarray.reduce((a, b) => a + b, 0)
        }
        return total

    } else if (a1 > b1) {
        if (c1 == null) {
            c1 = 1
        }
        nilaiarray = []
        for (let index = a1; index >= b1; index -= c1) {
            nilaiarray.push(index)
            total = nilaiarray.reduce((a, b) => a + b, 0)
        }
        return total

    }

}
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
