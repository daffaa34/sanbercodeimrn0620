var arr = []

function rangeWithStep(startNum, finishNum, step) {
    if (startNum == null || finishNum == null) {
        arr = []
        arr.push(-1)
        return arr

    } else if (startNum < finishNum) {
        arr = []
        for (let i = startNum; i <= finishNum; i +=step) {
            arr.push(i)

        }
        return arr

    } else if (startNum > finishNum) {
        arr = []
        for (let i = startNum; i >= finishNum; i-=step) {
            arr.push(i)

        }
        return arr

    }

}



console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11, 23, 11));
console.log(rangeWithStep(5,2,1));
console.log(rangeWithStep(29, 2, 4));
